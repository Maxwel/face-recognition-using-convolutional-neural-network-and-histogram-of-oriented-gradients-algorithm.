# -*- coding: utf-8 -*-
"""
Created on Fri Jun  1 19:51:17 2018

@author: Maxbro
"""
import keras
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D, Dropout
from keras.layers import Flatten
from keras.layers import Input
from keras.layers import concatenate
from keras.layers import Dense
from keras.models import load_model
from keras.optimizers import Adam
from keras.models import Model
from keras.utils import np_utils
from imutils import paths
import numpy as np
from sklearn.preprocessing import LabelEncoder 
import matplotlib.pyplot as plt
from skimage import feature
import imutils
import cv2
import os
import pandas as pd

##Define data with learning curve
# summarize history for accuracy
def plot_accuracy_against_epoch(model):
    plt.plot(model.history['acc'])
    plt.plot(model.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper right')
    plt.show()

# summarize history for loss
def plot_loss_against_epoch(model):
    plt.plot(model.history['loss'])
    plt.plot(model.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper right')
    plt.show()

def image_to_feature_vector(image, size=(128, 128)):
	return cv2.resize(image, size)

print("[INFO] describing images...")
imagePathsTrain = list(paths.list_images("training"))
imagePathsTest = list(paths.list_images("single"))

#%%
###################################### CNN PREPROCESSING
def load_image_cnn(dataset):
    data = []
    labels = []
    
    if dataset == imagePathsTrain:
        print("[INFO] Train images...")
    else:
        print("[INFO] Test images...")
    for (i, imagePath) in enumerate(dataset):
        image = cv2.imread(imagePath)
        #image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        label = imagePath.split(os.path.sep)[-1].split("_0")[0]
    
        features = image_to_feature_vector(image)
        data.append(features)
        labels.append(label)
        if i > 0 and i % 100 == 0:
            print("[INFO] processed {}/{}".format(i, len(dataset)))
            
    le = LabelEncoder()
    labels = le.fit_transform(labels)
    
    data = np.array(data) / 255.0
    labels = np_utils.to_categorical(labels, 10)
    
    return np.array(data), np.array(labels)


##Split data into training and testing
print("[INFO] constructing training/testing split...")
X_train_cnn , y_train_cnn = load_image_cnn(imagePathsTrain)
X_test_cnn, y_test_cnn = load_image_cnn(imagePathsTest)

#%%
def image_load_hog(dataset):
    data_hog = []
    labels_hog = []
    if dataset == imagePathsTrain:
        print("[INFO] Train images...")
    else:
        print("[INFO] Test images...")
    for imagePathHOG in dataset:

        make = imagePathHOG.split(os.path.sep)[-1].split("_0")[0]
        image = cv2.imread(imagePathHOG)
        features = image_to_feature_vector(image)
        gray = cv2.cvtColor(features, cv2.COLOR_BGR2GRAY)
        edged = imutils.auto_canny(gray)
     
        H = feature.hog(edged, orientations=9, pixels_per_cell=(16, 16),	
                        cells_per_block=(2, 2), transform_sqrt=True)
        
        data_hog.append(H)
        labels_hog.append(make)
        
        
    le = LabelEncoder()
    labels_hog = le.fit_transform(labels_hog)
    labels_hog = np_utils.to_categorical(labels_hog, 10)
    return np.array(data_hog), np.array(labels_hog)

X_train_hog , y_train_hog = image_load_hog(imagePathsTrain)
X_test_hog, y_test_hog = image_load_hog(imagePathsTest)

#%%
result = []
input_image_hog = Input(shape = (1764,))
input_image_cnn = Input(shape = (128, 128, 3))

"""DNN"""
dense_1_image_hog = Dense(1764, activation = 'relu')(input_image_hog)
drop_out_1 = Dropout(0.2)(dense_1_image_hog)
dense_2_image_hog = Dense(1000, activation = 'relu')(drop_out_1)
dense_3_image_hog = Dense(800, activation = 'relu')(dense_2_image_hog)
drop_out_2 = Dropout(0.2)(dense_3_image_hog)

"""CNN"""
conv_1_image_cnn = Convolution2D(32, (3, 3), input_shape = (32, 32, 3), activation = 'relu')(input_image_cnn)
pooling_1_image_cnn = MaxPooling2D(pool_size = (2,2))(conv_1_image_cnn)

conv_2_image_cnn = Convolution2D(64, (3, 3), activation = 'relu')(pooling_1_image_cnn)
pooling_2_image_cnn = MaxPooling2D(pool_size = (2,2))(conv_2_image_cnn)
drop_out_3 = Dropout(0.2)(pooling_2_image_cnn)

flatten_image_cnn = Flatten()(drop_out_3)

merged_tensor = concatenate([flatten_image_cnn,drop_out_2], axis=1)

"""Fully Connected Layer"""
hidden_layer_1 = Dense(units = 1024, activation = 'relu')(merged_tensor)
drop_out_4 = Dropout(0.2)(hidden_layer_1)
hidden_layer_2 = Dense(units = 512, activation = 'relu')(drop_out_4)
hidden_layer_3 = Dense(units = 256, activation = 'relu')(hidden_layer_2)

"""Output Layer"""
output = Dense(10, activation='softmax')(hidden_layer_3)

#%%
model = Model([input_image_hog,input_image_cnn],output)

adam = Adam(lr=0.001)
 
model.compile(optimizer=adam, loss='categorical_crossentropy', metrics=['accuracy'])
model.summary()
log = model.fit([X_train_hog, X_train_cnn], y_train_cnn, epochs = 50, validation_data = ([X_test_hog,X_test_cnn], y_test_cnn), batch_size = 50)
acc = log.history['acc'][-1]
val_acc = log.history['val_acc'][-1]
loss_model = log.history['loss'][-1]
val_loss = log.history['val_loss'][-1]
#%%
save_data = ["DNN","Relu","Relu", "0.2","0.2","relu", "CNN", "Relu","Relu", "Relu","0.2","0.2", "64", "64", acc, val_acc, loss_model, val_loss]
            
result.append(save_data)

data_frame = pd.DataFrame(result)
data_frame.to_csv("DataTunningActivation9010test.csv")
model.save("modelCombinationData8020test.h5")
#%%
model = load_model("modelCombinationData8020test.h5")
plot_accuracy_against_epoch(log)
plot_loss_against_epoch(log)

#%%
score = model.evaluate([X_test_hog,X_test_cnn], y_test_cnn, verbose=0)
print('Test score:', score[0])
print('Test accuracy:', score[1])

#%% Predict

model = load_model("modelCombinationData8020test.h5")
hasil = model.predict([X_test_hog,X_test_cnn])
#%%
hasil = np.argmax(hasil, axis=1)
#%% Predict
data = ['George W Bush', 'Gerhard Schroeder', 'Hugo Chavez', 
        'Jacques Chirac', 'Jean Chretien', 'John Ashcroft', 
        'Junichiro_Koizumi', 'Serena_Williams', 'Tony_Blair', 
        'Vladimir_Putin']

data_i = []

for i in hasil:
    print(data[i])
    data_i.append(data[i])
    
save = pd.DataFrame(data_i)
save.to_csv('outgabsingle.csv') 